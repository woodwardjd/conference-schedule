# Conference Schedule

I created this plugin for the HighEdWeb Association regionals to display their schedule online.

You can see the plugin in action on [the HighEdWeb New England website](http://ne16.highedweb.org/schedule/).

I plan to add more features in the near future.
